`use strict`;

// ## Завдання

// - Створити сторінку, яка імітує стрічку новин соціальної мережі [Twitter](https://twitter.com/).

// #### Технічні вимоги:

//  - При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
//    - `https://ajax.test-danit.com/api/json/users`
//    - `https://ajax.test-danit.com/api/json/posts`
//  - Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
//  - Кожна публікація має бути відображена у вигляді картки (приклад в папці), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
//  - На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу `https://ajax.test-danit.com/api/json/posts/${postId}`. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
//  - Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти [тут](https://ajax.test-danit.com/api-pages/jsonplaceholder.html).
//  - Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
//  - Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас `Card`. При необхідності ви можете додавати також інші класи.

// #### Необов'язкове завдання підвищеної складності

//  - Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
//  - Додати зверху сторінки кнопку `Додати публікацію`. При натисканні на кнопку відкривати модальне вікно, в якому користувач зможе ввести заголовок та текст публікації. Після створення публікації дані про неї необхідно надіслати в POST запиті на адресу:  `https://ajax.test-danit.com/api/json/posts`. Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному порядку). Автором можна присвоїти публікації користувача з `id: 1`.
//  - Додати функціонал (іконку) для редагування вмісту картки. Після редагування картки для підтвердження змін необхідно надіслати PUT запит на адресу `https://ajax.test-danit.com/api/json/posts/${postId}`.

const USER = "https://ajax.test-danit.com/api/json/users";
const POST = `https://ajax.test-danit.com/api/json/posts`;
const root = document.querySelector(`#root`);

class Request {
  request(url) {
    return fetch(url).then((response) => {
      return response.json();
    });
  }
}
let request = new Request();
let user = request.request(USER).then((data) => {
  return data;
});
let post = request.request(POST).then((data) => {
  return data;
});

class Card {
  createDeleteButton(userId, container) {
    let closeBtn = document.createElement(`button`);
    closeBtn.classList.add(`close__btn`);
    closeBtn.textContent = `Delete`;

    closeBtn.addEventListener(`click`, () => {
      fetch(`https://ajax.test-danit.com/api/json/posts/${userId}`, {
        method: "DELETE",
      }).then((response) => {
        if (response.status === 200) {
          container.remove();
        }
      });
    });
    return closeBtn;
  }
  createPostElement({ userId, title, body, name, username, email }) {
    let container = document.createElement(`div`);
    container.classList.add("container");

    let user = document.createElement(`p`);
    user.classList.add(`user__name`);
    user.textContent = `User: ${name}; User_nick_name: ${username}; Email: ${email}`;

    let titlePost = document.createElement(`p`);
    titlePost.classList.add(`title__post`);
    titlePost.textContent = `Theme: ${title}`;

    let bodyPost = document.createElement(`p`);
    bodyPost.classList.add(`body__post`);
    bodyPost.textContent = body;

    let closeBtn = this.createDeleteButton(userId, container);

    container.append(user, titlePost, bodyPost, closeBtn);

    return container;
  }

  render(data) {
    const [userArray, postArray] = data;
    let fragment = document.createDocumentFragment();

    userArray.forEach(({ id, name, username, email }) => {
      postArray.filter(({ userId, title, body }) => {
        if (userId === id) {
          let postElement = this.createPostElement({
            userId,
            title,
            body,
            name,
            username,
            email,
          });
          fragment.append(postElement);
        }
      });
    });

    return fragment;
  }
}
let card = new Card();

Promise.all([user, post]).then((data) => {
  root.append(card.render(data));
});
